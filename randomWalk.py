import numpy as np
from scipy.spatial.distance import cosine as cos, euclidean

def updateW(W,x,decay,alpha,S,x0):
    for (i,j), _ in np.ndenumerate(W):
        local = x[i] - (1 - alpha) * x0[i]
        transition = S[j][i] @ (x[j] - (1-alpha) * x0[j])
        delta = decay * cos(local,transition)
        W[i][j] =  W[i][j] + delta
        # print('ΔW:',delta)

def getnewP(P,W,S):
    W_hat = np.zeros(W.shape)
    P_new = np.zeros(P.shape)
    for (i,j),_ in np.ndenumerate(W):
        W_hat[i][j] =  W[i][j] / sum(W[i])
    for i in range(W.shape[0]):
        P_new[i] = np.add.reduce([ w * (sji @ p @ sij) for w,sji,p,sij in zip(W_hat[i], S[i,:],P,S[i])])

    #print ('ΔP',euclidean(P.flatten(),P_new.flatten()))
    return P_new

def updateX(x,P,alpha,x0,K):
    old = np.copy(x)
    for i in range(K):
    #calculate xi^t+1
        x[i] = alpha*(P[i] @ x[i]) + (1 - alpha)*x0[i]
    #print('Δx',euclidean(old.flatten(),x.flatten()))

#query_node is a tuple -> (layer_q, node)
def random_walk(transition_matrix, cross_transitions, tolerance, 
        query_node, decay_factor, restart_factor, T, K):
    
    
    u_q,layer_q = query_node
    network_column_size = transition_matrix[layer_q].shape[0]
 
    eq = np.zeros(network_column_size)
    eq[u_q] = 1

    # Init xi(0) -> either xi or eq
    x = np.ndarray((K,network_column_size))
 
    for i in range(K):
        if i == layer_q:
            x[i] = eq
        else:
            x[i] = cross_transitions[layer_q][i] @ eq

    W = np.eye(K,K) #I

    x0 = x

    for t in range(T):
        transition_matrix = getnewP(transition_matrix,W,cross_transitions)
        updateX(x,transition_matrix,restart_factor,x0,K)
        updateW(W,x,decay_factor,restart_factor,cross_transitions,x0)
    return x