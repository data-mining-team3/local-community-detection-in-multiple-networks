import readData
import randomWalk
import numpy as np

def extractP(network,K,Vsizes):
    # P = np.zeros(len(network))
    P = np.array([np.eye(size) for i,size in zip(range(K),Vsizes)])
    for layerid,layer in network.items():
        for node,edges in layer.items():
            for (target,weight) in edges:
                P[layerid][node][target] = weight

    return P


def get_probs():
    K = 10
    N = 92
    networs = readData.getGraph()
    P = extractP(networs,K,[N,N,N,N,N,N,N,N,N,N])
    S = np.array([[np.eye(N) for _ in range(K)] for _ in range(K)])

    return randomWalk.random_walk(P,S,None,(1,1),0.8,0.7,30,K)



# K = len(networs.keys())

# P = np.zeros((3,3))

# ff = np.frompyfunc(lambda a,b:a+b, 2,1)
# np.multiply.reduce([2,3,5])


# P = np.array([[[1,2],[3,4]],[[5,6],[7,8]]])
# W = np.array([[1,5],[1,3]])
# K = 2


