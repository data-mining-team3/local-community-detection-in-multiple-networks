from collections import defaultdict

def normalize(minimax,layer):
    mini,maxi = minimax[layer]
    return lambda x:(x-mini)/(maxi-mini)


def getGraph():
    MultiGraph = {}
    layer = -1
    minimax = {}
    with open("networks_pre.txt") as file:   
        for row in file:
            if row.isspace(): break
            if row.startswith('-'):
                layer += 1
                MultiGraph[layer]=defaultdict(list)
                minimax[layer] = None
            else:
                node,targetnode,weight = row.split('\t')
                weight = int(weight.strip())
                MultiGraph[layer][int(node)].append((int(targetnode),weight))

                if minimax[layer]:  
                    mini,maxi = minimax[layer]
                else:
                    mini = weight
                    maxi = weight

                mini = mini if mini > weight else weight
                maxi = maxi if maxi < weight else weight
                minimax[layer] = (mini,maxi)
    for layerid,layer in MultiGraph.items():
        norm = normalize(minimax,layerid)
        for node in layer:
            layer[node] = list(map(lambda n:(n[0],norm(n[1])),layer[node]))
    return MultiGraph


