import readData

import random
import numpy as np
from walker import get_probs


def get_volume_of_set(graph, set_of_nodes, net, same_weight = False):
    """Calculate the volume of a subset of nodes 'S' """
    """from a network 'net'"""
    """Inputs: list of nodes, network No., and if """
    """ edge {a,b}{b,a} have same weight or not."""
    
    degrees_sum = 0
    if same_weight:
        for node in set_of_nodes:
            degree = len(graph[net][node])
            degrees_sum += degree
    else:
        for node in set_of_nodes:
            for t in graph[net][node]:
                if (t[0] in set_of_nodes):
                    degrees_sum += 2       # need confirm given dataset
                else:
                    degrees_sum +=1
    
    # edges from nodes outside the subset
    nodes = [k for k in graph[net]]
    
    out_of_subset = set(nodes).difference(set(set_of_nodes))
    edges_from_out = 0
    
    for node in out_of_subset:
        for t in graph[net][node]:
            if (t[0] in set_of_nodes):
                    edges_from_out += 1
    
    return degrees_sum + edges_from_out

def get_edge_boundary(graph, set_of_nodes, net):
    """"Get the edge boundary of a set in network 'net'."""
    """Inputs: list of nodes, network No., if directed edge or not."""
    
    edge_boundary = 0
    for node in set_of_nodes:
        for t in graph[net][node]:
            if t[0] not in set_of_nodes:
                edge_boundary += 1
    
    # edges from nodes outside the subset
    nodes = [k for k in graph[net]]
    out_of_subset = set(nodes).difference(set(set_of_nodes))
    edges_from_out = 0
    
    for node in out_of_subset:
        for t in graph[net][node]:
            if (t[0] in set_of_nodes):
                    edges_from_out += 1

    return edge_boundary + edges_from_out


def edges_per_network(graph):
    """Get a list of numbers of edges in each network inside a graph."""
    
    edges_per_net = []
    net_nbr = len(graph.keys()) # number of networks
    
    for i in range(net_nbr):
        nodes = [k for k in graph[i]]
        sums = 0
        for node in nodes:
            sums += len(graph[i][node])
        edges_per_net.append(sums)
    
    return edges_per_net
    
    
def get_conductance(graph, set_of_nodes, net, same_weight=False):
    """ Computes the conductance of a subset S of nodes in network."""
    
    volume_s = get_volume_of_set(graph, set_of_nodes, net, same_weight)
    edge_boundary_s = get_edge_boundary(graph, set_of_nodes, net)
    edges_in_net = edges_per_network(graph)[net]
    min_degrees = min(volume_s, (2*edges_in_net - volume_s))
    conductance = edge_boundary_s/min_degrees
    
    return conductance


def local_cmty(graph, set_of_nodes, net, same_weight=False):
    """Get a list of nodes forming a local community"""
    """inputs: multigraph, a set of L ranked nodes, """
    """  network No(layer), if edge {a,b}{b,a} have same weight"""
    
    # number corresponding to L elements of a converged score vector
    l = len(set_of_nodes)  
    c_list = []  # list of conductance
    
    for i in range(1,l+1):
        nodeSubset = set_of_nodes[0:i]
        #print(nodeSubset)
        c = get_conductance(graph, nodeSubset, net, same_weight)
        c_list.append(c)
    
    #print(c_list)
    min_c = min(c_list)
    c_index = c_list.index(min_c)
    #print(min_c)
    l_cmty = set_of_nodes[:c_index+1]    

    return l_cmty


def get_fOne_score(detected_cmnty, truth_cmnty):
    """Compute micro F1 score for a detected community"""
    """receives two list of nodes as input"""
    
    detected_set = set(detected_cmnty)
    truth_set = set(truth_cmnty)
    nodes_detected = len(detected_set)
    nodes_truth = len(truth_set)
    
    d_t_intersect = len(detected_set.intersection(truth_set))
    precision =  d_t_intersect / nodes_detected
    recall_ = d_t_intersect / nodes_truth
    
    fOne_score = 2 * precision * recall_ / (precision + recall_)
    
    return fOne_score
    

def get_macro_fOne_score(fOne_scores):
    """Compute Macro-F1 score for all detected communities"""
    """Input: a list of F1 scores"""
    
    return  mean(fOne_scores)
    


if __name__ == "__main__":
    
    multiGr = readData.getGraph()
    random.seed(5)
    node_probs = get_probs()
    communities = []
    network = 0
    net_nodes = multiGr[network].keys()


    N = 40

    for walk in node_probs:
        aux = list(np.ndenumerate(walk))
        most_probable_nodes = sorted(aux, key=lambda x: x[1], reverse=True)
        local_comnty = local_cmty(multiGr, [i[0][0] for i in most_probable_nodes][:40], False)
        communities.append(local_comnty)

    
    print("\n\n")
    for c in communities:
        print(c)
        

